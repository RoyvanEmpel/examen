<?php 

  /**
   * 
   * Url Ophalen daaruit de dir verwijderen waar we in zitten en dan alle gekke tekens (quotes enzo) uit de link halen.
   * 
   * Ook exploden we alle subonderdelen in parts om er doorheen te kunnen loopen
   * 
   */

  $currentDir = explode("/", dirname(__FILE__))[count(explode("/", dirname(__FILE__)))-1];
  $parts = explode("/", str_replace($currentDir, "", ltrim($_SERVER['REQUEST_URI'], '/')));

  $class=DEFAULT_CLASS;
  $method=DEFAULT_METHOD;
  $params=[];
  foreach ($parts as $layer => $part) {
    $part = preg_replace('/[^A-Za-z0-9\-]/', '', $part);
    if (!empty(str_replace('/', '', $part))) {
      if (class_exists($part) && $class == DEFAULT_CLASS && $method == DEFAULT_METHOD) {
        $class = strtolower($part);
        $method = "";
      } elseif (method_exists($class, $part)) {
        $method = strtolower($part);
      } elseif ($class != DEFAULT_CLASS && ($method != DEFAULT_METHOD || $method != "")) {
        $params[] = $part;
      }
    }
  }

  /**
   * 
   * Kijken of de gebruiker ingelogd is en zoja dan verifieer de gegevens in de session
   * 
   */
  
  $Auth = new Auth();
  if (isset($_SESSION['login']['user']) && !empty($_SESSION['login']['user'])) {
    if (!$Auth->verify_login()) {
      session_destroy();
      header("Refresh:0");
    }
  }

  /**
   * 
   * Checkt of het een ajax request is. In het geval dat het admin_ajax is dan kijken we of de user er permissies voor heeft
   * 
   */

  if ($class == 'ajax') {
    (new Ajax())->{$method}(implode(", ", $params));
    die();
  } elseif ($class == 'admin_ajax') {
    if ($_SESSION['login']['user']['role'] == "Administrator") {
      (new Admin_ajax())->{$method}(implode(", ", $params));
      die();
    }
  }

  /**
   * 
   * Redirect de gebruiker naar de goede pagina. Probeer jij op een /nl pagina te komen dan wordt er gecheckt of dat wel mag.
   * 
   */


  if (empty($method)) {
    if ($class == 'nl' && (isset($_SESSION['login']['user']['role']) && $_SESSION['login']['user']['role'] != "Administrator") || !isset($_SESSION['login']['user']['role'])) {
      $class=DEFAULT_CLASS;
    }
    $method=DEFAULT_METHOD;
    $params=[];

    define('CURRENT_CLASS', $class);
    define('CURRENT_METHOD', $method);

    (new $class())->{$method}(implode(", ", $params));
  } else {
    if ($class == "nl") {
      if ((isset($_SESSION['login']['user']['role']) && $_SESSION['login']['user']['role'] != "Administrator") ||
        !isset($_SESSION['login']['user']['role'])) { //Als link /nl is en normale gebruiker
        $class=DEFAULT_CLASS;
        $method=DEFAULT_METHOD;
        $params=[];
      }
    }

    define('CURRENT_CLASS', $class);
    define('CURRENT_METHOD', $method);

    /**
     * 
     * Onderstaande functie roeps de juiste Class/Method Aan met meegegeven Parameters
     * 
    */

    call_user_func_array([new $class(), $method], $params);
  }
