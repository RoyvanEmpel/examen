// Dit js bestand wordt op elke pagina ingeladen.
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="tooltip"]').mouseleave(function () {
    $('.tooltip, .popover.fade').remove();
  });

  $('#data-table').DataTable({ dom: 'Bfrtip' });
  $('.select2').select2();
  $('#selectUser').select2();

  var fileChooser = document.getElementById('xml');

  var json = {};


  function createInputs()
  {
    $( json ).each(function( index, element ) {
      $('#hiddenInputs').append('<input type="hidden" name="' + index + '" value=\'' + JSON.stringify(element) + '\'>');
    });
  }

  function parseTextAsXml(text) {
    var parser = new DOMParser(),
      xmlDom = parser.parseFromString(text, "text/xml");

    json = xmlToJson(xmlDom).mboopen.spelers
    createInputs();
    console.log(json);
  }

  function waitForTextReadComplete(reader) {
    reader.onloadend = function (event) {
      var text = event.target.result;

      parseTextAsXml(text);
    }
  }

  function handleFileSelection() {
    var file = fileChooser.files[0],
      reader = new FileReader();

    waitForTextReadComplete(reader);
    reader.readAsText(file);
  }

  fileChooser.addEventListener('change', handleFileSelection, false);

  // Changes XML to JSON
  function xmlToJson(xml) {

    // Create the return object
    var obj = {};

    if (xml.nodeType == 1) { // element
      // do attributes
      if (xml.attributes.length > 0) {
        obj["@attributes"] = {};
        for (var j = 0; j < xml.attributes.length; j++) {
          var attribute = xml.attributes.item(j);
          obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
        }
      }
    } else if (xml.nodeType == 3) { // text
      obj = xml.nodeValue;
    }

    // do children
    if (xml.hasChildNodes()) {
      for (var i = 0; i < xml.childNodes.length; i++) {
        var item = xml.childNodes.item(i);
        var nodeName = item.nodeName;
        if (typeof (obj[nodeName]) == "undefined") {
          obj[nodeName] = xmlToJson(item);
        } else {
          if (typeof (obj[nodeName].push) == "undefined") {
            var old = obj[nodeName];
            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(xmlToJson(item));
        }
      }
    }
    return obj;
  };
});

function redirect(value, parameters = '') {
  location.href = value;
}

function NumFormat(nStr, var1 = ',', var2 = '.', var3 = '00', var4 = ',') {
  nStr = Math.round((nStr) * 100) / 100;
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = (x[1] ? (x.length > 0 ? var1 + x[1] + (x[1].length == 1 ? '0' : '') : '') : var4 + var3);
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + var2 + '$2');
  }
  return x1 + x2;
}