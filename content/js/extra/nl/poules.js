

function addPlayer(poule){
  $('#noUsers').remove();
  var id = $('#selectUser').children("option:selected").val();
  var name = $('#selectUser').children("option:selected").html();
  if (!$('#user_' + id).length) {
    // user_<?=$user->user_id; ?>

    console.log(id);
    console.log(name);

    $('#tbody_' + poule).append(
      '<tr id="tr_<?=$poule->poule_id; ?>_<?=$user->user_id; ?>">' +
        '<td>' + name + '</td>' +
        '<td class="text-right"><span class="btn btn-sm btn-danger" onclick="removePlayer(' + id + ', ' + poule + ')"><i class="mdi mdi-delete"></i></span></td>' +
      '</tr>'
    );

    $('#hiddenInputs_' + poule).append('<input type="hidden" id="user_' + id + '" name="users[' + poule + '][]" value="' + id + '">');
  }
}

function removePlayer(id, poule)
{
  $('#user_' + id + '').remove();
  $('#tr_' + poule + '_' + id).remove();
}

function annulModal()
{
  window.location = window.location.href;
}
