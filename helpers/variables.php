<?php

//print_pre Functie voor het makkelijk printen van arrays en objecten
if (!function_exists('print_pre')) {
 /**
  * @param mixed $var
  * @param boolean $die
  * 
  * @return void
  */
  function print_pre($var, $die = false)
  {

    echo '<pre>';
    print_r($var);
    echo '</pre>';

    if ($die) {
      die();
    }

  }
}

if (!function_exists('betweenNr')) {
  /**
  * @param int $nr1
  * @param int $nr2
  * @param int $nr3
  * 
  * @return boolean
  */
  function betweenNr($nr1, $nr2, $nr3) {
    if ($nr1 >= $nr2 && $nr1 <=$nr3) {
      return true;
    } else {
      return false;
    }
  }
}

if (!function_exists('get_string_between')) {
  /**
  * @param string $string
  * @param string $start
  * @param string $end
  * 
  * @return string
  */
  function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
  }
}

if (!function_exists('getClosest')) {
 /**
  * @param mixed $search
  * @param array $arr
  * 
  * @return void
  */
  function getClosest($search, $arr = []) 
  {
    if ($arr !== []) {
      $closest = null;
      foreach ($arr as $item) {
        if ($closest === null || abs($search - $closest) > abs($item - $search)) {
            $closest = $item;
        }
      }
      return $closest;
    } else {
      return false;
    }
  }
}

if (!function_exists('generateRandomString')) {
  function generateRandomString($length = 25) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }
}

if (!function_exists('afkorten'))
{
  function afkorten($string, $length)
  {
    if (mb_strlen($string) > $length) {
      $string = mb_substr($string, 0, $length - 3) . '...';
    }
    return $string;
  }
}

if (!function_exists('image')) {
  function image($image)
  {
    return "data:image/png;base64," . base64_encode(file_get_contents("content/images/" . $image));
  }
}

if (!function_exists('success')) {
  function success($message, $title="Success!")
  {
    if (!isset($_SESSION['messages']['success'])) $_SESSION['messages']['success'] = [];
    $_SESSION['messages']['success'][]=[
      "title" => $title,
      "message" => $message,
    ];
  }
}

if (!function_exists('notice')) {
  function notice($message, $title="PHP Notice")
  {
    if (!isset($_SESSION['messages']['dark'])) $_SESSION['messages']['dark'] = [];
    $_SESSION['messages']['dark'][]=[
      "title" => $title,
      "message" => $message,
    ];
  }
}

if (!function_exists('warning')) {
  function warning($message, $title="PHP Warning:")
  {
    if (!isset($_SESSION['messages']['warning'])) $_SESSION['messages']['warning'] = [];
    $_SESSION['messages']['warning'][]=[
      "title" => $title,
      "message" => $message,
    ];
  }
}

if (!function_exists('error')) {
  function error($message, $title="PHP Error:")
  {
    if (!isset($_SESSION['messages']['danger'])) $_SESSION['messages']['danger'] = [];
    $_SESSION['messages']['danger'][]=[
      "title" => $title,
      "message" => $message,
    ];
  }
}

if (!function_exists('EngToNldDateNames')) {
  function engToNldDateNames($date)
  {
    //volledige dagnamen
    $day_en = array(
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday"
    );
    $day_nl = array(
        "Maandag",
        "Dinsdag",
        "Woensdag",
        "Donderdag",
        "Vrijdag",
        "Zaterdag",
        "Zondag",
    );
    $dateString = str_replace($day_en, $day_nl, $date);
    
    //volledige maandnamen
    $month_en = array(
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    );
    $month_nl = array(
      "Januari",
      "Februari",
      "Maart",
      "April",
      "Mei",
      "Juni",
      "Juli",
      "Augustus",
      "September",
      "Oktober",
      "November",
      "December",
    );
    $dateString = str_replace($month_en, $month_nl, $dateString);
    
    //korte dagnamen
    $day_en_short = array(
        "Mon",
        "Tue",
        "Wed",
        "Thu",
        "Fri",
        "Sat",
        "Sun"
    );
    $day_nl_short = array(
      "Ma.",
      "Di.",
      "Wo.",
      "Do.",
      "Vr.",
      "Za.",
      "Zo."
    );
    $dateString = str_replace($day_en_short, $day_nl_short, $dateString);
    
    //korte maandnamen
    $month_en_short = array(
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    );
    $month_nl_short = array(
      "Jan.",
      "Feb.",
      "Mar.",
      "Apr.",
      "Mei",
      "Jun.",
      "Jul.",
      "Aug.",
      "Sep.",
      "Okt.",
      "Nov.",
      "Dec."
    );
    $dateString = str_replace($month_en_short, $month_nl_short, $dateString);
    
    return $dateString;
  }
}
