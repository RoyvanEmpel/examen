#BELANGRIJK

### Om ervoor te zorgen dat het systeem de juiste functions aanroept moet het onderste ingesteld worden!

In de **C:\xampp\apache\httpd.conf**

Verifieer dat **LoadModule rewrite_module modules/mod_rewrite.so** geen **#** ervoor heeft.  
Op linux is het makkelijker om **sudo a2enmod rewrite && sudo systemctl restart apache2** te doen


Ook moet er bij het onderstaande **None** => **All**

**`<Directory "C:/xampp/htdocs">`**  
&nbsp;&nbsp;&nbsp;&nbsp;`Options Indexes FollowSymLinks Includes ExecCGI`  
&nbsp;&nbsp;&nbsp;&nbsp;`AllowOverride None`  
&nbsp;&nbsp;&nbsp;&nbsp;`Require all granted`  
**`</Directory>`**

wordt
**`<Directory "C:/xampp/htdocs">`**  
&nbsp;&nbsp;&nbsp;&nbsp;`Options Indexes FollowSymLinks Includes ExecCGI`  
&nbsp;&nbsp;&nbsp;&nbsp;`AllowOverride All`  
&nbsp;&nbsp;&nbsp;&nbsp;`Require all granted`  
**`</Directory>`**
