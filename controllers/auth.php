<?php

	class Auth {
		private $db;

		function __construct() { //Arrays aanmaken en $db variable vullen
			if (!isset($_SESSION['login']['errors'])) {
				$_SESSION['login'] = array();
				$_SESSION['login']['errors'] = array();
			}
			$this->db = new DB;
		}

		public function verify_login() //Functie voor het checken of de user ingelogd is.
		{
			$input = $_SESSION['login']['user'];

			$email = strtolower(trim($input['email']));
			$password = trim($input['password']);

			if (empty($email) || empty($password)) {
				return false;
			} else {

				try {
					$user = $this->db->query('SELECT * FROM users WHERE email = "' . $email . '"')->row();
					if (!empty($user)) {
						if ($password === $user->password || (isset($_SESSION['passUpdate']) && $_SESSION['passUpdate'] == 1)) {
							if (isset($_SESSION['passUpdate'])) { unset($_SESSION['passUpdate']); }
							$this->loginUser($user, false);
							return true;
						}
					}

					$this->addError('Niet ingelogd.');
					return false;
				} catch (\Throwable $th) {
					throw $th;
				}
				
			}
		}

		public function login() //Functie inlog te verifieren
		{
			$input = $_POST;

			$email = strtolower(trim($input['email']));
			$password = trim($input['password']);

			if (empty($email) || empty($password)) {
				$this->addSuccess('Lege velden zijn niet toegestaan.');
			} else {

				try {
					$user = $this->db->query('SELECT * FROM users WHERE email = "' . $email . '"')->row();
					if (!empty($user)) {
						if ($this->hash($password) === strtoupper($user->password)) {
							$this->loginUser($user, true);
						} else {
							$this->addError('Het ingevoerde email of wachtwoord is onjuist.');
						}
					} else {
						$this->addError('Het ingevoerde email of wachtwoord is onjuist.');
					}
				} catch (\Throwable $th) {
					throw $th;
				}

			}
		}

		public function register() //functie voor registreren
		{
			$input = $_POST;
			if (isset($input['email']) && !empty($input['email']) && 
					isset($input['firstname']) && !empty($input['firstname']) && 
					isset($input['lastname']) && !empty($input['lastname']) && 
					isset($input['password']) && !empty($input['password'])) {
	
				try {
					$email = strtolower(trim($input['email']));
					$firstname = strtolower(trim($input['firstname']));
					$lastname = strtolower(trim($input['lastname']));
					$password = $this->hash(trim($input['password']));
		
					$emailCheck = $this->db->query('SELECT * FROM users WHERE email = "' . $email . '"')->row();
					$role_id = $this->db->query('SELECT `role_id` FROM `roles` WHERE `role` = "Guest"')->row()->role_id;

					if (empty($emailCheck)) {
						$this->db->insert('users', [
																				'role_id' => $role_id,
																				'email' => $email, 
																				'firstname' => $firstname, 
																				'lastname' => $lastname, 
																				'password' => $password
																			]);

						$this->addSuccess('Account successvol aangemaakt!');
					} else {
						$this->addError('Email wordt al gebruikt.');
					}
				} catch (\Throwable $th) {
					throw $th;
				}
			} else {
				$this->addError('Niet alle velden zijn correct ingevuld.');
				
			}
		}

		public function loginUser($user, $reload=true) //Login de user
		{
			if (!isset($_SESSION['login']['user']) || empty($_SESSION['login']['user'])) {
				$_SESSION['login']['user']=[];
			}

			$_SESSION['login']['user'] = (array)$user;
			$_SESSION['login']['user']['loggedIn'] = true;
			$_SESSION['login']['user']['role'] = $this->db->query('SELECT `role` FROM `roles` WHERE `role_id` = "' . $user->role_id . '"')->row()->role;

			if ($reload) {
				header("Location: ". DEFAULT_DIR);
			}
		}

		public function updateUser()
		{
			$this->db->update('users', $_POST, ['user_id' => $_SESSION['login']['user']['user_id']]);
		}

		private function addError($error) { //Error toevoegen om te laten zien
			$_SESSION['login']['errors'][] = $error;
		}

		private function addSuccess($message) { //Success toevoegen om te laten zien
			$_SESSION['login']['success'][] = $message;
		}

		public function hash($pass) //Functie voor sha512 Hashen en strtoupper
		{
			return strtoupper(hash('sha512', $pass));
		}

	}

	new Auth();