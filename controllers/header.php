<?php

class Header {

  public static $extraCSS;

  function __construct()
  {
    self::$extraCSS;
  }

  public static function extra($fileLocation) //Functie voor extra css inladen
  {
    if(is_array($fileLocation)) {
      foreach ($fileLocation as $file) {
        self::$extraCSS[] = $file;
      }
    } else {
      self::$extraCSS[] = $fileLocation;
    }
  }

  public static function unloadCSS($unloadFile = null) //Alle momenteel ingelade css unloaden
  {
    if ($unloadFile) {
      foreach (self::$extraCSS as $index => $file) {
        if ($file == $unloadFile){
          unset(self::$extraCSS[$index]);
        }
      }
    } else {
      self::$extraCSS = [];
    }
  }

  private static function head() //head data terug geven
  {
    if (isset(View::$data['title'])) {
      return'
              <meta charset="UTF-8"/>
              <title>' . View::$data['title'] . ' - ' . SITE_TITLE. '</title>
              <link rel="shortcut icon" href="' . FAVICON . '" type="image/ico">
            ';
    }

    return '
            <meta charset="UTF-8"/>
            <title>'.SITE_TITLE. ' - ' .$_SESSION['page'].'</title>
            <link rel="shortcut icon" href="' . FAVICON . '" type="image/ico">
          ';

  }

  private static function styles() //Alle styles/JS inladen
  {
    return '
            <link name="materialdesignicons" rel="stylesheet" href="//cdn.materialdesignicons.com/5.4.55/css/materialdesignicons.min.css">
            <style name="bootstrap">' . file_get_contents('content/css/bootstrap/bootstrap.min.css') . '</style>
            <style name="bootstrap-grid">' . file_get_contents('content/css/bootstrap/bootstrap-grid.min.css') . '</style>
            <style name="bootstrap-reboot">' . file_get_contents('content/css/bootstrap/bootstrap-reboot.min.css') . '</style>
            <style name="jquery-ui.min">' . file_get_contents('content/css/jquery-ui/jquery-ui.min.css') . '</style>
            <style name="jquery-ui.structure">' . file_get_contents('content/css/jquery-ui/jquery-ui.structure.min.css') . '</style>
            <style name="jquery-ui.theme">' . file_get_contents('content/css/jquery-ui/jquery-ui.theme.min.css') . '</style>
            <style name="datatables">' . file_get_contents('content/css/datatables/datatables.min.css') . '</style>
            <link href="https://cdn.jsdelivr.net/npm/siiimple-toast/dist/style.css" rel="stylesheet">
            <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css" integrity="sha512-3//o69LmXw00/DZikLz19AetZYntf4thXiGYJP6L49nziMIhp6DVrwhkaQ9ppMSy8NWXfocBwI3E8ixzHcpRzw==" crossorigin="anonymous" />
            <script name="jquery">' . file_get_contents('content/js/jquery/jquery.js') . '</script>
            <script name="bootstrap-bundle">' . file_get_contents('content/js/bootstrap/bootstrap.bundle.min.js') . '</script>
            <script name="datatables">' . file_get_contents('content/js/datatables/datatables.min.js') . '</script>
            <script name="sweetalert2">' . file_get_contents('content/js/sweetalert2/sweetalert2.all.min.js') . '</script>
            <script src="https://cdn.jsdelivr.net/npm/siiimple-toast/dist/siiimple-toast.min.js"></script>
            <script src="https://kit.fontawesome.com/d68d772542.js" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
          ';
  }

  public static function load() //Opbouwen van de header.
  {
    echo '<html lang="en"><head>';
    echo self::head();
    echo self::styles();

    self::extra('extra/custom.css');

    if (!empty(self::$extraCSS)) {
      foreach (self::$extraCSS as $file) {
        if (file_exists('content/css/' . $file)) {
          echo '<style name="' . basename($file, '.css') . '">' . file_get_contents('content/css/' . $file) . '</style>';
        } else {
          echo '<script>alert("'.$file.' can\'t be loaded.");</script>';
        }
      }
    }
      
    echo '</head><body>';
  }

}

new Header();