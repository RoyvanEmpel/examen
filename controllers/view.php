<?php

class View {

  public static $data;
  public static function load($file = '', $data=[]) //Inladen van een pagina + header en footer laden.
  {
    if (isset($data['data'])) {
      foreach ($data['data'] as $key => $byte) {
        $data[$key] = $byte;
      }
  
      unset($data['data']);
    }
    self::$data = $data;

    Header::load();

    try {
      if (!empty($file)) {
        if (is_array($file)) {
          foreach ($file as $f) {
            if (file_exists('views/' . $f . '.php')) {
              require_once('views/' . $f . '.php');
            } else {
              echo "
                <script>
                  alert('$f pagina niet gevonden.');
                  location.reload();
                </script>
              ";
            }
            require_once('views/' . $f . '.php');
          }
        } else {
          if (file_exists('views/' . $file . '.php')) {
            require_once('views/' . $file . '.php');
          } else {
            echo "
              <script>
                alert('$file pagina niet gevonden.');
                location.reload();
              </script>
            ";
          }

        }
      }
    } catch (\Throwable $th) {
      throw $th;
    }

    Footer::load();
  }

  public static function getFile($file) 
  {
    return file_get_contents('views/' . $file . '.php');
  }

}