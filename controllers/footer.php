<?php

class Footer{

  private static $extraJS;

  function __controller() {
    self::$extraJS = [];
  }

  public static function extra($fileLocation) //Functie voor extra JS inladen
  {
    if(is_array($fileLocation)) {
      foreach ($fileLocation as $file) {
        self::$extraJS[] = $file;
      }
    } else {
      self::$extraJS[] = $fileLocation;
    }
  }
  
  public static function unloadJS($unloadFile = null) //Alle momenteel ingelade js unloaden
  {
    if ($unloadFile) {
      foreach (self::$extraJS as $index => $file) {
        if ($file == $unloadFile){
          unset(self::$extraJS[$index]);
        }
      }
    } else {
      self::$extraJS = [];
    }
  }

  public static function load() //Inladen van de JS
  {
    echo '
      </body>
    ';
    
    self::extra('extra/custom.js');

    if (!empty(self::$extraJS)) {
      foreach (self::$extraJS as $file) {
        if (file_exists('content/js/' . $file)) {
          echo '<script name="' . basename($file, '.js') . '">' . file_get_contents('content/js/' . $file) . '</script>';
        } else {
          echo '<script>alert("'.$file.' can\'t be loaded.");</script>';
        }
      }
    }

    $messageTypes = ["success", "dark", "warning", "danger"];

    $count = 0;
    foreach ($messageTypes as $type) {
      if (isset($_SESSION['messages'][$type])) {
        $count+= count($_SESSION['messages'][$type]);
      }
    }

    echo "<script>";
      echo "
        $(function () {
          siiimpleToast = siiimpleToast.setOptions({
            container: 'body',
            class: 'toast',
            position: 'top|right',
            margin: 15,
            delay: 100,
            duration: 15000,
            style: { },
          });
      ";
      $i=0;
      foreach ($messageTypes as $type) {
        if (isset($_SESSION['messages'][$type])) {
          foreach ($_SESSION['messages'][$type] as $message) {
            $i++;
            echo "setTimeout(function(){";
            echo "siiimpleToast.message(`<b class='text-$type'>" . $message["title"] . "</b><br /> " . $message["message"] . "`);";
            if ($count > 1) {
              if ($count == $i) {
                for ($i=0; $i < $count; $i++) { 
                  echo "}, 750);";
                }
              }
            } else {
              echo "}, 750);";
            }
          }
        }
      }

      echo "});";
    echo "</script>";

    unset($_SESSION['messages']);

    echo '</html>';
  }

}