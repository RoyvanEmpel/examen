<?php

  //Session starten
  session_start();

  // ERRORS AANZETTEN ADV LOKAAL/LIVE
  if ($_SERVER['SERVER_ADDR'] == '127.0.0.1' || $_SERVER['SERVER_NAME'] == 'localhost') {
    ini_set('display_errors', 1); 
    ini_set('display_startup_errors', 1); 
    error_reporting(E_ALL);
  } else {
    ini_set('display_errors', 0); 
    ini_set('display_startup_errors', 0); 
    error_reporting(0);
  }
  ini_set('memory_limit', '1024M');

  //Constants definen om ze overal te kunnen gebruiken.
  define('SITE_TITLE', 'Examen');
  define('FAVICON', image("logo.png"));

  define('DEFAULT_CLASS', 'main');
  define('DEFAULT_METHOD', 'login');
  define('DEFAULT_PAGE', '/' . DEFAULT_CLASS . '/' . DEFAULT_METHOD);
  define('DEFAULT_DIR', '/' . explode("/", ltrim($_SERVER['REQUEST_URI'], '/'))[0]);

  include_once('controllers/header.php');
  include_once('controllers/footer.php');
  include_once('controllers/view.php');
  
  if (!array_key_exists('HTTP_MOD_REWRITE', $_SERVER)) { // Check if mod_rewrite is turned on
    $_SESSION['page'] = "ERROR";
    Header::load();
    include('views/mod_rewrite.php');
  } else {
    //Inladen van helpers / controllers 
    require_once('controllers/db.php');
    
    include_once('controllers/auth.php');

    require_once('controllers/ajax.php');
    require_once('controllers/admin_ajax.php');
    require_once("controllers/main.php");
    require_once("controllers/nl.php");

    include_once('redirector.php'); //Roep de juiste Methode/Functie aan adv link
  }