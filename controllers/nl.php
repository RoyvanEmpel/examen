

<?php

class Nl
{

  private $db;
  function __construct() // Hier kijken we of het een Ajax request is en zoniet dan checken we of de user ingelogd is en verifieren we de login.
  {
    $this->db = new DB();
  }

  public function redirect($link = "") //Deze functie kijkt of je de juiste permissies hebt voordat hij redirect
  {
    header("Location: " . DEFAULT_DIR . "$link");
  }

  public function index() //Landings Pagina
  {
    $this->redirect('/nl/dashboard');
  }


  public function login() //Login Pagina
  {
    $this->redirect('/login');
  }

  public function dashboard($param1=false, $param2=false, $param3=false)
  {
    try {
      $data = new stdClass();

      Header::extra('extra/nl/partials/sidebar.css'); //Css Sidebar Inladen
      Footer::extra('extra/nl/partials/sidebar.js'); //Js Sidebar Inladen
      
      View::load(
        [
          'nl/partials/header', //Inladen Header (sidebar)
          'nl/dashboard', //Pagina Inladen
          'nl/partials/footer' //Inladen Footer
        ],
        [
          'title' => 'Beheerders paneel',
          'data' => $data // Stuur data mee naar het bestand
        ]
      );
    } catch (\Throwable $th) {
      error($th);
    }
  }

  public function tournaments()
  {
    try {
      $data = new stdClass();
      include_once('models/tournaments_m.php');
      $this->tournaments_m = new Tournaments_m();

      $data->tournaments = $this->tournaments_m->getAll();

      Header::extra('extra/nl/partials/sidebar.css'); //Css Sidebar Inladen
      Footer::extra('extra/nl/partials/sidebar.js'); //Js Sidebar Inladen
      
      View::load(
        [
          'nl/partials/header', //Inladen Header (sidebar)
          'nl/tournaments', //Pagina Inladen
          'nl/partials/footer' //Inladen Footer
        ],
        [
          'title' => 'Toernooien overzicht',
          'data' => $data // Stuur data mee naar het bestand
        ]
      );
    } catch (\Throwable $th) {
      error($th);
    }
  }
  
  public function tournament($tournament_id=false, $action="edit")
  {
    try {
      $data = new stdClass();
      include_once('models/tournaments_m.php');
      $this->tournaments_m = new Tournaments_m();

      if (!empty($_POST)) {
        if ($tournament_id) {
          $this->tournaments_m->editTournament($tournament_id, $_POST);
        } else {
          $this->tournaments_m->addTournament($_POST);
        }
        $this->redirect('/nl/tournaments');
      }

      if ($tournament_id) {
        $data->tournament = $this->tournaments_m->get($tournament_id);
      }

      Header::extra('extra/nl/partials/sidebar.css'); //Css Sidebar Inladen
      Footer::extra('extra/nl/partials/sidebar.js'); //Js Sidebar Inladen
    
      View::load(
        [
          'nl/partials/header', //Inladen Header (sidebar)
          'nl/tournament_' . $action, //Pagina Inladen
          'nl/partials/footer' //Inladen Footer
        ],
        [
          'title' => 'Toernooi ' . ($action == "edit" ? ($tournament_id ? 'aanpassen' : 'toevoegen') : "bekijken"),
          'data' => $data // Stuur data mee naar het bestand
        ]
      );
    } catch (\Throwable $th) {
      error($th);
    }
  }

  public function nextRound($tournament_id=false) {
    try {
      if (!$tournament_id) { $this->redirect(); } // Als er geen tournament_id is redirect naar homepage.
      include_once('models/players_m.php'); // Inladen Players Model
      include_once('models/matches_m.php'); // Inladen Matches Model

      $data = new stdClass();

      $this->players_m = new Players_m(); // Players Class Aanroepen
      $this->matches_m = new Matches_m(); // Matches Class Aanroepen

      $data->round = ($this->matches_m->getRound($tournament_id)->round ?? 1); // Haal de huidige ronden op.
      $data->matches = $this->matches_m->getByTournament($tournament_id, $data->round); // Haal alle matches a.d.h.v. tournament op

      foreach ($data->matches as $match) {
        if (!$match->winner_id && $match->player_1_id && $match->player_2_id) { // Check om te kijken of alle matches zijn ingevult/gespeeld
          $this->redirect('/nl/results/'.$tournament_id); // In het geval dat niet alles is ingevuld redirect terug naar de resultaten pagina.
          break;
        }
      }

      $data->dummydPlayer = (array)$this->players_m->getAllDummyd($tournament_id, $data->round); // Haal alle spelers op die niet in ronde 1 hoefde te spelen.
      $data->winners = (array)$this->players_m->getWinners($tournament_id, $data->round); // Haal alle winners van ronde 1 op.

      $players=[];
      foreach ($data->dummydPlayer as $player) { // Players met dummys in players array zetten
        if (!empty($player->player_1) || empty($player->player_1) && empty($player->player_2)) {
          $players[] = $player->player_1;
        } else {
          $players[] = $player->player_2;
        }
      }

      foreach ($data->winners as $player_id) { // Winnaars in de players array zetten.
        $players[] = $player_id->winner_id;
      }

      for ($i=0; $i < count($players); $i+=2) { // Maar wedstrijden aan voor de spelers.
        $this->matches_m->addMatch([
          "tournament_id" => $tournament_id,
          "round" => $data->round+1,
          "player_1" => isset($players[$i]) ? $players[$i] : NULL,
          "player_2"=> isset($players[$i+1]) ? $players[$i+1] : NULL,
        ]);
      }

      $this->redirect('/nl/results/'.$tournament_id); // Terug naar het overzicht
    } catch (\Throwable $th) {
      error($th);
    }
  }

  public function closeSignin($tournament_id) // Dit is de functie voor het aanmaken dan de eerste ronde.
  {
    try {
      if (!$tournament_id) { $this->redirect(); }

      include_once('models/players_m.php'); // Inladen Players Model
      include_once('models/matches_m.php'); // Inladen Matches Model

      $data = new stdClass();

      $this->players_m = new Players_m(); // Players Class Aanroepen
      $this->matches_m = new Matches_m(); //Matches Class Aanroepen
      $data->players = (array)$this->players_m->getAll($tournament_id); //Alle players ophalen bij het tournament

      $done = false;
      $pouleSize=4;
      $countPlayers=count($data->players);
      while ($done == false) { // Een while loop die kijkt hoe groot de poule aan gebruikers moet zijn.
        if ($pouleSize > $countPlayers) { // Als de poule size meer dan het aantal spelers is dan kappen we de while loop af en weten we hoeveel dummys nodig zijn.
          $done=true;
          $dummys = $pouleSize - $countPlayers;
          for ($i=0; $i < $dummys; $i++) { // Hier voegen we de dummys toe en shufflen we de players array een aantal keer
            $data->players[] = NULL;
            shuffle($data->players);
          }
        } else {
          $pouleSize *= 2; // Poule Size vergroten omdat het niet genoeg is voor onze spelers
        }
      }

      for ($i=0; $i < $pouleSize; $i+=2) { 
        $this->matches_m->addMatch([
          "tournament_id" => $tournament_id,
          "round" => 1,
          "player_1" => isset($data->players[$i]->player_id) ? $data->players[$i]->player_id : NULL, // Player of Dummy Invoegen
          "player_2"=> isset($data->players[$i+1]->player_id) ? $data->players[$i+1]->player_id : NULL, // Player of Dummy Invoegen
        ]);
      }

      $this->redirect("/nl/results/$tournament_id");
    } catch (\Throwable $th) {
      error($th);
    }
  }

  public function players($tournament_id = false)
  {
    try {
      if (!$tournament_id) { $this->redirect(); }
      $data = new stdClass();
      include_once('models/players_m.php'); // Inladen Players Model
      include_once('models/matches_m.php'); // Inladen Matches Model
      $data->tournament_id = $tournament_id;

      $this->players_m = new Players_m(); // Players Class Aanroepen
      $this->matches_m = new Matches_m(); // Matches Class Aanroepen
      $data->round = ($this->matches_m->getRound($tournament_id)->round ?? 1); // Haal de huidige ronden op.

      $data->tournament_count = count((array)$this->matches_m->getByTournament($tournament_id, $data->round)); // Matches Tellen

      if (isset($_POST["players"]) && !empty($_POST["players"])) { // In het geval dat er iets in de post['players'] staat. (Dit is als de Spelers definitief worden toegevoegd)

        foreach ($_POST["players"] as $player) { // Voor elke player gaan we toevoegen

          if (!empty($player)) {
            $player = [
              'tournament_id' => $tournament_id,
              'player_id' => $player['ID'],
              'firstname' => $player['Voornaam'],
              'lastname' => $player['Achternaam'],
              'school_id' => $player['SchoolID'],
            ]; // Player op de goede manier opbouwen

            if (isset($arr['Tussenvoegsels'])) { //In het geval dat er tussenvoegsels zijn voegen we die ook toe.
              $player['tussenvoegsel'] = ((array)$arr['Tussenvoegsels'])['#text'];
            }

            $this->players_m->addPlayer($player); // Voeg de speler toe aan de Database
          }

        }

        $this->redirect("/nl/players/$tournament_id"); // Redirecten naar Aanmeldings Overzicht
      } elseif (isset($_POST['delete'])) { // Speler verwijderen uit het systeem
        $this->players_m->deletePlayer($_POST['delete'], $tournament_id);
      } elseif (isset($_POST['close'])) { // Aanmeldingen sluiten
        $this->closeSignin($tournament_id);
      } elseif (!empty($_POST)) { // In het geval dat er een xml is geupload komen we hier terecht
        $data->temp_players = [];
        foreach ($_POST as $json) {
          $arr = (array)json_decode($json); // Decodeer de data en zet het in een array.
          unset($arr['#text']);

          foreach ($arr as &$item) {
            $item = $item->{'#text'};
          }

          $data->temp_players[] = $arr;
        }
      }

      $data->players = $this->players_m->getAll($tournament_id, $data->round); //Alle players ophalen a.d.h.v. het tournament

      Header::extra('extra/nl/partials/sidebar.css'); //Css Sidebar Inladen
      Footer::extra('extra/nl/partials/sidebar.js'); //Js Sidebar Inladen
      
      View::load(
        [
          'nl/partials/header', //Inladen Header (sidebar)
          'nl/players', //Pagina Inladen
          'nl/partials/footer' //Inladen Footer
        ],
        [
          'title' => 'Aanmeldingen overzicht',
          'data' => $data // Stuur data mee naar het bestand
        ]
      );
    } catch (\Throwable $th) {
      error($th);
    }
  }

  public function schools()
  {
    try {
      $data = new stdClass();
      include_once('models/schools_m.php'); // Inladen Schools Model
      $this->schools_m = new Schools_m(); // Schools Class Aanroepen

      $data->schools = $this->schools_m->getAll();

      Header::extra('extra/nl/partials/sidebar.css'); //Css Sidebar Inladen
      Footer::extra('extra/nl/partials/sidebar.js'); //Js Sidebar Inladen
      
      View::load(
        [
          'nl/partials/header', //Inladen Header (sidebar)
          'nl/schools', //Pagina Inladen
          'nl/partials/footer' //Inladen Footer
        ],
        [
          'title' => 'Scholen overzicht',
          'data' => $data // Stuur data mee naar het bestand
        ]
      );
    } catch (\Throwable $th) {
      error($th);
    }
  }

  public function school($school_id=false)
  {
    try {
      $data = new stdClass();
      include_once('models/schools_m.php'); //Inladen schools Model
      $this->schools_m = new Schools_m(); //Schools Aanroepen

      if (is_array($school_id) && isset($school_id[1]) && $school_id[1] == 'delete') { //Check voor School 'Verwijderen'
        $this->schools_m->delSchool($school_id[0]); //School 'verwijderen'
        $this->redirect('/nl/schools');
      } elseif (is_array($school_id)) {
        $this->redirect('/nl/schools'); // In het geval dat de link niet klopt redirect dan terug.
      }

      if (!empty($_POST)) {
        if ($school_id) {
          $this->schools_m->editSchool($school_id, $_POST);
        } else {
          $this->schools_m->addSchool($_POST);
        }
        $this->redirect('/nl/schools');
      }

      if ($school_id) {
        $data->school = $this->schools_m->get($school_id);
      }

      Header::extra('extra/nl/partials/sidebar.css'); //Css Sidebar Inladen
      Footer::extra('extra/nl/partials/sidebar.js'); //Js Sidebar Inladen
      
      View::load(
        [
          'nl/partials/header', //Inladen Header (sidebar)
          'nl/school_edit', //Pagina Inladen
          'nl/partials/footer' //Inladen Footer
        ],
        [
          'title' => 'School ' . ($school_id ? 'aanpassen' : 'toevoegen'),
          'data' => $data // Stuur data mee naar het bestand
        ]
      );
    } catch (\Throwable $th) {
      error($th);
    }
  }


  public function player($input=false) // tournament_id / player_id
  {
    try {
      if (!$input) { $this->redirect(); }
      $data = new stdClass();
      include_once('models/players_m.php'); //Players model inladen
      include_once('models/schools_m.php'); //Schools model inladen

      $this->players_m = new Players_m(); //Players Class Aanroepen
      $this->schools_m = new Schools_m(); //Schools Class Aanroepen

      $data->schools = $this->schools_m->getAll(); //Haal alle scholen op
      $data->tournament_id = $input[0];
      if( isset($input[1]) ) { //Kijken of een Player_id is meegegeven
        $player_id = $input[1];
      }

      if (!empty($_POST)) { //In het geval dat een player in de post staan editen/adden we hem aan de aanmeldingslijst
        if (isset($player_id)) {
          $this->players_m->editPlayer($player_id, $_POST);
        } else {
          $_POST['tournament_id'] = $input[0];
          $this->players_m->addPlayer($_POST);
        }
        $this->redirect('/nl/players/'.$input[0]); //Altijd terug redirecten naar de tournament na het toevoegen/aanpassen
      }

      if (isset($player_id)) { // Als er een player_id is dan halen we de user op
        $data->player = $this->players_m->get($player_id);
      }

      Header::extra('extra/nl/partials/sidebar.css'); //Css Sidebar Inladen
      Footer::extra('extra/nl/partials/sidebar.js'); //Js Sidebar Inladen
      
      View::load(
        [
          'nl/partials/header', //Inladen Header (sidebar)
          'nl/player_edit', //Pagina Inladen
          'nl/partials/footer' //Inladen Footer
        ],
        [
          'title' => 'Speler ' . (($input[1] ?? 0) ? 'aanpassen' : 'toevoegen'), //Titel aanpassen a.d.h.v. Edit/Add
          'data' => $data // Stuur data mee naar het bestand
        ]
      );
    } catch (\Throwable $th) {
      error($th);
    }
  }
  
  public function results($tournament_id)
  {
    try {
      if (!$tournament_id) { $this->redirect(); } // Geen tournament geselecteerd? redirect naar home

      $data = new stdClass();
      include_once('models/matches_m.php'); //Inladen model
      $this->matches_m = new Matches_m(); //Class aanroepen

      if (!empty($_POST['matches'])) {

        foreach ($_POST['matches'] as $match_id => $match) {
          if (isset($match['player_1_score']) && isset($match['player_2_score'])) {
            if (!isset($match['winner_1']) && !isset($match['winner_2'])) { //In hetgeval dat er geen winnaar geselecteerd is kijken we of player score 1 groter is dan player score 2 zonet en score1 en 2 zijn het zelfde dan kiezen we random wie er wint.
              $input['winner_id'] = ($match['player_1_score'] > $match['player_2_score'] ? $match['player_id_1'] : ($match['player_1_score'] != $match['player_2_score'] ? $match['player_id_2'] : [$match['player_id_1'], $match['player_id_2']][array_rand([$match['player_id_1'], $match['player_id_2']])] ));
            } else {
              $input['winner_id'] = ($match['winner_1'] ?? $match['winner_2']); //Gekozen winnaar
            }
          }

          $input['player_1_score'] = $match['player_1_score']; //Ingevulde score p1
          $input['player_2_score'] = $match['player_2_score']; //Ingevulde score p2

          $this->matches_m->editScores($match_id, $input); //Scores opslaan
        }

        $this->redirect('/nl/results/'.$tournament_id); //Redirect naar de matches pagina
      }

      if ($tournament_id) {
        $data->tournament_id = $tournament_id;
        $data->round = ($this->matches_m->getRound($tournament_id)->round ?? 1); // Haal de huidige ronden op.
        $data->matches = $this->matches_m->getByTournament($tournament_id, $data->round); //Haal alle matches a.d.h.v. tournament op

        $data->roundDone=true;
        foreach ($data->matches as $match) {
          if (!$match->winner_id && $match->player_1_id && $match->player_2_id) {
            $data->roundDone=false;
            break;
          }
        }
      }

      Header::extra('extra/nl/partials/sidebar.css'); //Css Sidebar Inladen
      Footer::extra('extra/nl/partials/sidebar.js'); //Js Sidebar Inladen
      
      View::load(
        [
          'nl/partials/header', //Inladen Header (sidebar)
          'nl/results', //Pagina Inladen
          'nl/partials/footer' //Inladen Footer
        ],
        [
          'title' => 'Uitslagen',
          'data' => $data // Stuur data mee naar het bestand
        ]
      );
    } catch (\Throwable $th) {
      error($th);
    }

  }

  public function result($tournament_id)
  {
    try {
      if (!$tournament_id) { $this->redirect(); } // Geen tournament geselecteerd? redirect naar home

      $data = new stdClass();
      include_once('models/matches_m.php'); //Inladen model
      $this->matches_m = new Matches_m(); //Class aanroepen

      if ($tournament_id) {
        $data->tournament_id = $tournament_id;
        $data->round = ($this->matches_m->getRound($tournament_id)->round ?? 1); // Haal de huidige ronden op.

        for ($i=1; $i <= $data->round; $i++) { 
          $data->matches[] = $this->matches_m->getByTournament($tournament_id, $i); //Haal alle matches a.d.h.v. tournament op
        }
      }

      Header::extra('extra/nl/partials/sidebar.css'); //Css Sidebar Inladen
      Footer::extra('extra/nl/partials/sidebar.js'); //Js Sidebar Inladen
      
      View::load(
        [
          'nl/partials/header', //Inladen Header (sidebar)
          'nl/result_view', //Pagina Inladen
          'nl/partials/footer' //Inladen Footer
        ],
        [
          'title' => 'Uitslagen',
          'data' => $data // Stuur data mee naar het bestand
        ]
      );
    } catch (\Throwable $th) {
      error($th);
    }
  }

  public function logout() //Logout... redelijk voor zich zelf sprekend.
  {
    session_destroy(); // Sessie wegflikkeren
    $this->redirect(); // Redirect en laat de redirector het maar zelf uitzoeken.
  }

}
