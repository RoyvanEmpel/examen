

<?php

class Main
{

  private $db;
  function __construct() // Hier kijken we of het een Ajax request is en zoniet dan checken we of de user ingelogd is en verifieren we de login.
  {
    $this->db = new DB();
  }

  public function redirect($link = "") //Deze functie kijkt of je de juiste permissies hebt voordat hij redirect
  {
    header("Location: " . DEFAULT_DIR . "$link");
  }

  // public function index() //Landings Pagina
  // {
  //   Header::extra(['extra/partials/header.css', 'extra/index.css', 'extra/partials/footer.css']);
  //   Footer::extra('extra/index.js');
  //   View::load(['partials/header', 'index', 'partials/footer'], ['title' => 'Hoofdpagina']);
  // }

  public function login() //Login Pagina
  {
    $this->auth = new Auth();
    if (!isset($_SESSION['login']['user']['loggedIn']) || $_SESSION['login']['user']['loggedIn'] === false) {
      if (isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['password'])) { // Checken Of Het Registeren of Login Is.
        $this->auth->register();
        $this->redirect(DEFAULT_DIR.'/login');
      } elseif (isset($_POST['email']) && isset($_POST['password'])) {
        $this->auth->login();
      }
    } else {
      if (!$this->auth->verify_login()) {
        $this->logout();
      } else {
        $this->redirect('/nl/dashboard');
      }
    }

    Header::extra('extra/login.css');
    Footer::extra('extra/login.js');
    View::load('login', ['title' => 'Inloggen/Registreren']);
  }

  public function register() //Register Pagina Redirect Naar Login. (Pagina veranderd wel)
  {
    $this->login();
  }

  public function logout() //Logout... redelijk voor zich zelf sprekend.
  {
    session_destroy();
    $this->redirect();
  }
}
