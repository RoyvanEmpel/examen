<?php
/**
 * Database
 */

include_once('conn.php');

class DB
{
	private $conn;
	private $result;

	private function conn() //Connectie aanmaken
	{
		try {
			$this->conn = new PDO("mysql:host=".Conn::$host.";dbname=".Conn::$db, Conn::$user, Conn::$pass);
		} catch (\Throwable $th) {
			throw $th;
		}
	}

	public function getConn() //Connectie terug geven (Speciale gevallen)
	{
		return $this->conn;
	}

	public function row() //1 Rij terug geven als een object
	{
		$result = $this->row_array();
		if (!empty($result)) {
			return (object)$result;
		} else {
			return [];
		}
	}

	public function row_array() //1 Rij terug geven als een array
	{
		try {
			if (!empty($this->result)) {
				return $this->result->fetch(PDO::FETCH_ASSOC);
			}
			return [];
		} catch (\Throwable $th) {
			throw $th;
		}
		
	}

	public function result() //Alle rijen in een object terug geven
	{
		try {
			$result = new stdClass();
			foreach ($this->result_array() as $key => $value) {
				$result->{$key} = (object)$value;
			}
			return $result;
		} catch (\Throwable $th) {
			throw $th;
		}
	}

	public function result_array() //Alle rijen in een array terug geven
	{
		try {
			return $this->result->fetchAll(PDO::FETCH_ASSOC);
		} catch (\Throwable $th) {
			throw $th;
		}
	}

	public function query($query) //Query functie voor het ophalen van data
	{
		if (!isset($this->conn)) {
			$this->conn();
		}

		try {
			$stmt = $this->conn->prepare($query);
			$stmt->execute();

			$this->result = $this->conn->query($query);
			return $this;
		} catch (\Throwable $th) {
			error("Faulthy query: $query");
		}
	}
	
	public function delete($table, $wheres) //Delete functie voor het verwijderen van data
	{
		if (!isset($this->conn)) {
			$this->conn();
		}

		$query = "DELETE FROM `$table`";

		$query .= " WHERE";
		$i=1;
		foreach($wheres as $index => $where) {
			if ($i>1) { $query .= " AND";}
			$query .= " `$index` = '$where'";
			$i++;
		}
		
		try {
			$stmt = $this->conn->prepare($query);
			$stmt->execute();
		} catch (\Throwable $th) {
			error("Faulthy query: $query");
		}
	}

	public function insert($table, $input) //Insert functie voor het invoegen van data
	{
		if (!isset($this->conn)) {
			$this->conn();
		}

		$prefix = $values = [];
		foreach ($input as $key => $value) {
			$prefix[] = $key;
			$values[] = $value;
		}

		$query = "INSERT INTO `$table` (" . implode(", ", $prefix). ") VALUES ('" . implode("', '", $values). "')";

		try {
			$stmt = $this->conn->prepare($query);
			$stmt->execute();
			return $this->conn->lastInsertId();;
		} catch (\Throwable $th) {
			error("Faulthy query: $query");
		}
		
	}

	public function update($table, $update, $where) //Update functie voor het updaten van data
	{
		if (!isset($this->conn)) {
			$this->conn();
		}

		$query = "UPDATE `$table` SET ";

		$i=1;
		foreach ($update as $key => $value) {
			$query .= ($i == 1 ? '' : ',') . "`$key` = '$value'";
			$i++;
		}

		$query .= " WHERE";
		$i=1;
		foreach($where as $index => $where) {
			if ($i>1) { $query .= " AND";}
			$query .= " `$index` = '$where'";
			$i++;
		}
		
		try {
			$stmt = $this->conn->prepare($query);
			$stmt->execute();
		} catch (\Throwable $th) {
			error("Faulthy query: $query");
		}
		
	}
}
