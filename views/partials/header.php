<header>

  <img src="<?=image("logo.png"); ?>" alt="" width="auto" height="90px" class="m-l-20 m-t-20">

  <?php if (!isset($_SESSION['login']['user']['loggedIn']) || $_SESSION['login']['user']['loggedIn'] === false) { ?>
    <span id="loginbtn" onclick="redirect('login')" class="btn btn-info">Inloggen / Registreren</span>
  <?php } else { ?>
    <span id="loginbtn" onclick="redirect('logout')" class="btn btn-info">Uitloggen</span>

    <?php if (isset($_SESSION['login']['user']['loggedIn']) && $_SESSION['login']['user']['loggedIn'] === true && $_SESSION['login']['user']['role'] == "Administrator") { ?> 
      <span id="loginbtn" onclick="redirect('<?=DEFAULT_DIR; ?>/nl/dashboard')" class="btn btn-info">Beheerders Paneel</span>
    <?php } ?>

  <?php } ?>

  <nav class="navbar">
    <span <?=CURRENT_METHOD == "index" ? 'style="color: cyan;"' : ''?> onclick="redirect('index')" id="nav_home">Home</span>
  </nav>

</header>