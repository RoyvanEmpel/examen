<div class="container">
  <center>
    <h1><b>Aanmeldingen overzicht</b></h1>
  </center>
  <hr>
    <a class="btn btn-danger" href="<?=DEFAULT_DIR; ?>/nl/tournaments">Terug naar tournooien</a>

    <?php if (!isset($data['tournament_count']) || $data['tournament_count'] == 0) { ?>
      <div class="text-right">
        <a class="btn btn-success text-white" onclick="confirmClose()">Aanmelden Sluiten</a>
        <a class="btn btn-info text-white" data-toggle="modal" data-target="#xmlModal">Import XML</a>
        <a href="<?=DEFAULT_DIR; ?>/nl/player/<?=$data['tournament_id']; ?>" class="btn btn-success">Speler toevoegen</a>
      </div>
    <?php } else { ?>
      <span class="alert alert-info btn-block m-t-20">Aanmeldingen zijn gesloten.</span>
    <?php } ?>

    <div class="row m-t-30">

      <div class="col-12">

        <?php if (empty((array)$data['players'])) { ?>
          <span class="alert alert-danger btn-block text-center"> Geen spelers kunnen vinden. </span>
        <?php } else { ?>
          <table class="table table-striped table-hover">

            <thead>
              <th>Voornaam</th>
              <th>Tussenvoegsel</th>
              <th>Achternaam</th>
              <th>School</th>
              <th class="no-sort text-right">Opties</th>
            </thead>

            <tbody>
              <?php foreach ($data['players'] as $player) { ?>
                <tr>
                  <td><?=$player->firstname; ?></td>
                  <td><?=$player->tussenvoegsel; ?></td>
                  <td><?=$player->lastname; ?></td>
                  <td><?=$player->school_name ; ?></td>
                  <td class="text-right">
                    <form action="" method="post" class="m-0">
                      <a class="btn btn-xs btn-warning text-white" href="<?=DEFAULT_DIR; ?>/nl/player/<?=$player->tournament_id; ?>/<?=$player->player_id; ?>" data-toggle="tooltip" data-placement="top" title="Speler aanpassen"><i class="mdi mdi-pencil"></i></a>
                      <?php if (!isset($data['tournament_count']) || $data['tournament_count'] == 0) { ?>
                        <input type="hidden" name="delete" value="<?=$player->player_id; ?>">
                        <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Speler verwijderen"><i class="mdi mdi-delete"></i></button>
                      <?php } ?>
                    </form>
                  </td>
                </tr>
              <?php } ?>
            </tbody>

          </table>
        <?php } ?>

      </div>

    </div>

</div>

<div class="modal fade" id="xmlModal" tabindex="-1" role="dialog" aria-labelledby="xmlModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="xmlModalLabel">XML Importeren</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
          <div class="row">
            <div class="form-group">
              <input type="file" class="form-control" name="xml" id="xml" accept="text/xml">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="btn btn-primary" disabled onclick="$('#xmlForm').submit();" aria-disabled="true">Importeren</span>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuleren</button>
      </div>
    </div>
  </div>
</div>

<form action="" method="post" id="xmlForm">
  <div id="hiddenInputs"></div>
</form>

<div class="modal fade" id="tempPlayers" tabindex="-1" role="dialog" aria-labelledby="tempPlayersLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="tempPlayersLabel">Spelers Importeren</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
          <form action="" method="post" id="PlayerForm">
            
            <?php if (isset($data['temp_players'])) { ?>
              <?php foreach ($data['temp_players'] as $player) { ?>
                <div class="row" id="player_row_<?=$player['ID']; ?>">
                  <div class="col-6"><span><?=$player['Voornaam']; ?> <?=($player['Tussenvoegsels'] ?? ""); ?> <?=$player['Achternaam']; ?></span></div>
                  <div class="col-6 text-right"><span class="btn btn-danger btn-xs" onclick="removePlayer(<?=$player['ID']; ?>)"><i class="mdi mdi-delete"></i></span></div>
                  <?php foreach ($player as $key => $val) { ?>
                    <input type="hidden" id="player_<?=$player['ID']; ?>" name="players[<?=$player['ID']; ?>][<?=$key; ?>]" value="<?=$val; ?>">
                  <?php } ?>
                </div>
              <?php } ?>
            <?php } ?>

          </form>
        </div>
      </div>
      <div class="modal-footer">
        <span class="btn btn-primary" disabled onclick="$('#PlayerForm').submit();" aria-disabled="true">Opslaan</span>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuleren</button>
      </div>
    </div>
  </div>
</div>

<?php if (isset($data['temp_players'])) { ?>
<script>
  function removePlayer(id)
  {
    $('#player_row_' + id).remove();
  }

  $(document).ready(function(){
    $("#tempPlayers").modal('show');
  });
</script>
<?php } ?>

<script>
  function confirmClose()
  {
    Swal.fire({
      title: 'Weet je het zeker?',
      text: "Hierna kan je de aanmeldingen niet meer aanpassen en worden er automatisch spelers in de eerste ronde geplaatst!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ja, sluiten!',
      cancelButtonText: 'Annuleren'
    }).then((result) => {
      if (result.isConfirmed) {
        $('#formClose').submit();
      }
    })
  }
</script>

<form action="" method="post" id="formClose">
  <input type="hidden" name="close" value="<?=$data['tournament_id']; ?>">
</form>