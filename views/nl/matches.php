<div class="container">

  <center>
    <h1><b>Wedstrijden overzicht</b></h1>
  </center>
  <hr>

  <div class="text-right">
    <a href="<?=DEFAULT_DIR; ?>/nl/match" class="btn btn-success">Wedstrijden toevoegen</a>
  </div>

  <?php if (empty((array)$data['matches'])) { ?>
    <span class="alert alert-danger btn-block text-center"> Geen wedstrijden kunnen vinden. </span>
  <?php } else { ?>

    <div class="row m-t-30">

      <div class="col-12">
        <?php if (empty((array)$data['tournaments'])) { ?>
          <span class="alert alert-danger btn-block text-center"> Geen toernooien kunnen vinden. </span>
        <?php } else { ?>

          <div class="accordion" id="accordionExample">
            <?php foreach ($data['tournaments'] as $index => $tournament) { ?>
              <?php if (!empty((array)$tournament->matches)) { ?>
                <div class="card">
                  <div class="card-header" id="heading<?=$tournament->tournament_id; ?>">
                    <div class="row" data-toggle="collapse" data-target="#collapse<?=$tournament->tournament_id; ?>" aria-expanded="<?=$index == 0 ? 'true' : 'false' ; ?>" aria-controls="collapse<?=$tournament->tournament_id; ?>">
                      <div class="col-6">
                        <h2 class="mb-0">
                          <button class="btn btn-link btn-block text-left <?=$index == 0 ? '' : 'collapsed' ; ?>" type="button" >
                            <b>Toernooi:</b> <span data-toggle="tooltip" data-placement="top" title="<?=$tournament->tournament_description; ?>"><?=afkorten($tournament->tournament_description, 80); ?></span>
                          </button>
                        </h2>
                      </div>
                      <div class="col-6 text-right">
                        <span class="btn btn-info btn-sm">Wedstrijden bekijken</span>
                      </div>
                    </div>
                    
                  </div>

                  <div id="collapse<?=$tournament->tournament_id; ?>" class="collapse  <?=$index == 0 ? 'show' : '' ; ?>" aria-labelledby="heading<?=$tournament->tournament_id; ?>" data-parent="#accordionExample">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-4">
                          <b>Namen</b>
                        </div>
                        <div class="col-4">
                          <b>Scores</b>
                        </div>
                      </div>
                      <?php foreach ($tournament->matches as $match) { ?>
                        
                        <div class="row">
                          <div class="col-4">
                            Jos Bos - Barrie Konings
                          </div>
                          <div class="col-4">
                            1 - 4
                          </div>
                        </div>
                      <?php } ?>
                        
                    </div>
                  </div>
                </div>
              <?php } ?>
            <?php } ?>
          </div>
        <?php } ?>
      </div>

    </div>
  <?php } ?>
</div>