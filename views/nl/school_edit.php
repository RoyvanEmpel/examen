<div class="container">
  <div class="row">
    <div class="col-12 text-center">
      <h1><b>School <?= isset($data['school']) ? 'aanpassen' : 'toevoegen'; ?></b></h1>
      <hr>
    </div>
  </div>

  <form action="" method="post">
    <div class="row">
      <div class="col-12 p-r-10">
        <div class="form-group">
          <label for="school_name">Naam</label>
          <input type="text" name="school_name" id="school_name" class="form-control" value="<?=$data['school']->school_name ?? ''; ?>">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <hr>
      </div>
    </div>

    <div class="row">
      <div class="col-6">
        <a class="btn btn-danger" href="<?= DEFAULT_DIR; ?>/nl/schools"><i class="mdi mdi-backspace"></i> Annuleren</a>
      </div>
      <div class="col-6 text-right">
        <button type="submit" class="btn btn-success"><i class="mdi mdi-floppy"></i> Opslaan</button>
      </div>
    </div>
  </form>
</div>