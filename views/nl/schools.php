<div class="container">

  <center>
    <h1><b>Scholen overzicht</b></h1>
  </center>
  <hr>
  

    <div class="text-right">
      <a href="<?=DEFAULT_DIR; ?>/nl/school" class="btn btn-success">School toevoegen</a>
    </div>

    <div class="row m-t-30">

      <div class="col-12">

        <?php if (empty((array)$data['schools'])) { ?>
          <span class="alert alert-danger btn-block text-center"> Geen scholen kunnen vinden. </span>
        <?php } else { ?>
          <table id="data-table" class="table table-striped table-hover">

            <thead>
              <th>Name</th>
              <th>Aantal spelers</th>
              <th class="no-sort text-right">Opties</th>
            </thead>

            <tbody>
              <?php foreach ($data['schools'] as $school) { ?>
                <tr>
                  <td><?=$school->school_name; ?></td>
                  <td><?=$school->amount_players ?? 0; ?> Speler<?=$school->amount_players == 1 ? "" : "s"; ?></td>
                  <td class="text-right">
                    <a class="btn btn-xs btn-warning text-white" href="<?=DEFAULT_DIR; ?>/nl/school/<?=$school->school_id; ?>"><i class="mdi mdi-pencil"></i></a>
                    <a class="btn btn-xs btn-danger text-white" href="<?=DEFAULT_DIR; ?>/nl/school/<?=$school->school_id; ?>/delete"><i class="mdi mdi-delete"></i></a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>

          </table>
        <?php } ?>

      </div>

    </div>

</div>