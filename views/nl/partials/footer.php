    <div class="container">
      <div class="row">
        <div class="col-12">
          <hr>
          <div class="text-center">
            <span class="text-secondary">&copy; 2020-<?= date('Y'); ?> - Roy van Empel</span>
          </div>
          <hr>
        </div>
      </div>
    </div>
  </div>
</div>
