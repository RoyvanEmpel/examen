<div class="wrapper d-flex align-items-stretch">
  <nav id="sidebar" class="">
    <div class="custom-menu">
      <button type="button" id="sidebarCollapse" class="btn btn-primary">
        <i class="fa fa-bars"></i>
        <span class="sr-only">Toggle Menu</span>
      </button>
    </div>
    <div class="p-4">
      <span class="font-26">Adminpaneel</span>
      <ul class="list-unstyled components mb-5">
        <li>
          <a href="<?=DEFAULT_DIR; ?>/nl/dashboard" class="font-20 <?=(CURRENT_METHOD == "dashboard" ? 'text-knltb' : 'hover'); ?>"><i class="mdi mdi-home"></i> Home</a>
        </li>

        <li>
          <a href="<?=DEFAULT_DIR; ?>/nl/tournaments" class="font-20 <?=(in_array(CURRENT_METHOD, ["tournaments", "tournament", "players"]) ? 'text-knltb' : 'hover'); ?>"><i class="mdi mdi-seal"></i> Tournamenten</a>

          <a href="<?=DEFAULT_DIR; ?>/nl/tournament" class="m-l-15 <?=(CURRENT_METHOD == "tournament" && !isset($data['tournament']) ? 'text-knltb' : 'hover'); ?>" style="padding<?=isset($data['tournament']) ? '' : '-top'; ?>:0;"><i class="mdi mdi-subdirectory-arrow-right"></i> Tournament toevoegen</a>
          <?php if (CURRENT_METHOD == 'tournament' && isset($data['tournament'])) { ?>
            <a href="<?=DEFAULT_DIR; ?>/nl/tournament/<?=$data['tournament']->tournament_id; ?>" class="m-l-15 <?=(CURRENT_METHOD == "tournament" && isset($data['tournament']) ? 'text-knltb' : 'hover'); ?>" style="padding-top:0;"><i class="mdi mdi-subdirectory-arrow-right"></i> Tournament aanpassen</a>
          <?php } ?>
          <?php if (CURRENT_METHOD == 'players') { ?>
            <a href="<?=DEFAULT_DIR; ?>/nl/players" class="m-l-15 <?=(CURRENT_METHOD == "players" ? 'text-knltb' : 'hover'); ?>" style="padding-top:0;"><i class="mdi mdi-subdirectory-arrow-right"></i> Aanmeldingen</a>
          <?php } ?>
          <?php if (CURRENT_METHOD == 'results') { ?>
            <a href="<?=DEFAULT_DIR; ?>/nl/results" class="m-l-15 <?=(CURRENT_METHOD == "results" ? 'text-knltb' : 'hover'); ?>" style="padding-top:0;"><i class="mdi mdi-subdirectory-arrow-right"></i> Uitslagen</a>
          <?php } ?>
        </li>

        <li>
          <a href="<?=DEFAULT_DIR; ?>/nl/schools" class="font-20 <?=(CURRENT_METHOD == "schools" ? 'text-knltb' : 'hover'); ?>"><i class="mdi mdi-account-switch"></i> Scholen </a>

          <a href="<?=DEFAULT_DIR; ?>/nl/school" class="m-l-15 <?=(CURRENT_METHOD == "school" ? 'text-knltb' : 'hover'); ?>" style="padding<?=isset($data['school']) ? '' : '-top'; ?>:0;"><i class="mdi mdi-subdirectory-arrow-right"></i> School toevoegen</a>
          <?php if (CURRENT_METHOD == 'school' && isset($data['school'])) { ?>
            <a href="<?=DEFAULT_DIR; ?>/nl/school/<?=$data['school']->school_id; ?>" class="m-l-15 <?=(CURRENT_METHOD == "school" ? 'text-knltb' : 'hover'); ?>" style="padding-top:0;"><i class="mdi mdi-subdirectory-arrow-right"></i> School aanpassen</a>
          <?php } ?>
        </li>
        <li class="hover">
          <a href="<?=DEFAULT_DIR; ?>/logout" class="font-20"><i class="mdi mdi-logout"></i> Logout</a>
        </li>

      </ul>
    </div>
  </nav>

  <!-- Page Content  -->
  <div id="content" class="p-4 p-md-5 pt-5">