<div class="container">

  <center>
    <h1><b>Uitslagen ronde <?=$data['round']; ?></b></h1>
  </center>
  <hr>

  <div class="row">
    <div class="col-6">
      <a class="btn btn-danger" href="<?=DEFAULT_DIR; ?>/nl/tournaments">Terug naar tournooien</a>
      <a class="btn btn-info" href="<?=DEFAULT_DIR; ?>/nl/result/<?=$data['tournament_id']; ?>">Overzicht Tonen</a>
    </div>
    <div class="col-6 text-right">
      <?php if ($data['roundDone'] && !empty((array)$data['matches'])) { ?>
        <?php if (count((array)$data['matches']) != 1) { ?>
          <a class="btn btn-warning text-white" href="<?=DEFAULT_DIR; ?>/nl/nextRound/<?=$data['tournament_id']; ?>">Ronde <?=$data['round']+1; ?> aanmaken</a>
        <?php } ?>
      <?php } ?>
      <?php if (!empty((array)$data['matches'])) { ?>
        <span class="btn btn-success" onclick="$('#uitslagenForm').submit();">Uitslagen opslaan</span>
      <?php } ?>
    </div>
  </div>

    <div class="row m-t-30">

      <div class="col-12">

      <?php if (count((array)$data['matches']) == 1) { ?>
        <span class="alert alert-warning btn-block"><?=$data['matches']->{0}->firstname_w . " " . $data['matches']->{0}->tussenvoegsel_w . " " . $data['matches']->{0}->lastname_w; ?> is de winnaar!</span>
      <?php } ?>

      <?php if (!empty((array)$data['matches'])) { ?>
        <form action="" method="post" id="uitslagenForm">
          <table class="table table-striped table-hover">

            <thead>
              <th class="no-sort">Speler</th>
              <th class="no-sort">Score</th>
            </thead>

            <tbody>
              <?php foreach ($data['matches'] as $match) { ?>
                <?php if ($match->player_1_id && $match->player_2_id) { ?>
                  <tr>
                    <td>
                      <div class="form-check form-check-inline">
                        <input type="hidden" name="matches[<?=$match->match_id; ?>][player_id_1]" value="<?=$match->player_1_id; ?>">
                        <input class="form-check-input" type="checkbox" id="<?=$match->match_id; ?>_1" name="matches[<?=$match->match_id; ?>][winner_1]" value="<?=$match->player_1_id; ?>" <?=($match->player_1_id == $match->winner_id ? 'checked' : ''); ?>>
                        <label class="form-check-label" for="<?=$match->match_id; ?>_1"><?=$match->firstname_1; ?> <?=$match->tussenvoegsel_1; ?> <?=$match->lastname_1; ?></label>
                      </div><br>
                      <div class="form-check form-check-inline">
                        <input type="hidden" name="matches[<?=$match->match_id; ?>][player_id_2]" value="<?=$match->player_2_id; ?>">
                        <input class="form-check-input" type="checkbox" id="<?=$match->match_id; ?>_2" name="matches[<?=$match->match_id; ?>][winner_2]" value="<?=$match->player_2_id; ?>"<?=($match->player_2_id == $match->winner_id ? 'checked' : ''); ?>>
                        <label class="form-check-label" for="<?=$match->match_id; ?>_2"><?=$match->firstname_2; ?> <?=$match->tussenvoegsel_2; ?> <?=$match->lastname_2; ?></label>
                      </div>
                    </td>
                    <td style="padding:5px;">
                      <div class="form-group form-group-sm m-0">
                        <input type="number" style="width:50px;" name="matches[<?=$match->match_id; ?>][player_1_score]" value="<?=(isset($match->player_1_score) ? $match->player_1_score : ""); ?>"><br>
                        <input type="number" style="width:50px;"name="matches[<?=$match->match_id; ?>][player_2_score]" value="<?=(isset($match->player_2_score) ? $match->player_2_score : ""); ?>">
                      </div>
                    </td>
                  </tr>
                <?php } ?>
              <?php } ?>
            </tbody>

          </table>
        </form>
      <?php } else { ?>
        <hr>
        <span class="alert alert-danger btn-block">Geen wedstrijden gevonden</span>
      <?php } ?>

      </div>

    </div>

</div>