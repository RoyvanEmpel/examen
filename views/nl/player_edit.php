<div class="container">
  <div class="row">
    <div class="col-12 text-center">
      <h1><b>Speler <?= isset($data['player']) ? 'aanpassen' : 'toevoegen'; ?></b></h1>
      <hr>
    </div>
  </div>

  <form action="" method="post">
    <div class="row">
      <div class="col-5 p-r-10">
        <div class="form-group">
          <label for="firstname">Speler voornaam</label>
          <input type="text" name="firstname" id="firstname" class="form-control" value="<?=$data['player']->firstname ?? ''; ?>">
        </div>
      </div>
      <div class="col-2 p-l-10 p-r-10">
        <div class="form-group">
          <label for="tussenvoegsel">Tussenvoegsel</label>
          <input type="text" name="tussenvoegsel" id="tussenvoegsel" class="form-control" value="<?=$data['player']->tussenvoegsel ?? ''; ?>">
        </div>
      </div>
      <div class="col-5 p-l-10">
        <div class="form-group">
          <label for="lastname">Speler achternaam</label>
          <input type="text" name="lastname" id="lastname" class="form-control" value="<?=$data['player']->lastname ?? ''; ?>">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <div class="form-group">
          <select name="school_id" id="school_id" class="form-select btn-block select2"">
            <?php foreach ($data['schools'] as $school) { ?>
              <option value="<?=$school->school_id; ?>" <?=(($data['player']->school_id ?? NULL) == $school->school_id ? "selected" : ""); ?>><?=$school->school_name; ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <hr>
      </div>
    </div>

    <div class="row">
      <div class="col-6">
        <a class="btn btn-danger" href="<?= DEFAULT_DIR; ?>/nl/players/<?=$data['tournament_id']; ?>"><i class="mdi mdi-backspace"></i> Annuleren</a>
      </div>
      <div class="col-6 text-right">
        <button type="submit" class="btn btn-success"><i class="mdi mdi-floppy"></i> Opslaan</button>
      </div>
    </div>
  </form>
</div>