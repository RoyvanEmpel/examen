<div class="container">
  <div class="row">
    <div class="col-12 text-center">
      <h1><b>Tournament <?= isset($data['tournament']) ? 'aanpassen' : 'toevoegen'; ?></b></h1>
      <hr>
    </div>
  </div>

  <form action="" method="post">
    <div class="row">
      <div class="col-12">
        <div class="form-group">
          <label for="tournament_date">Tournament datum</label>
          <input type="date" name="tournament_date" id="tournament_date" class="form-control" value="<?= str_replace(' ', 'T', date('Y-m-d', strtotime((isset($data['tournament']) ? $data['tournament']->tournament_date : date('d-m-Y'))))); ?>">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <div class="form-group">
          <label for="tournament_description">Tournament beschrijving</label>
          <textarea name="tournament_description" id="tournament_description" rows="5" class="form-control"><?= (isset($data['tournament']) ? $data['tournament']->tournament_description : ''); ?></textarea>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <hr>
      </div>
    </div>

    <div class="row">
      <div class="col-6">
        <a class="btn btn-danger" href="<?= DEFAULT_DIR; ?>/nl/tournaments"><i class="mdi mdi-backspace"></i> Annuleren</a>
      </div>
      <div class="col-6 text-right">
        <button type="submit" class="btn btn-success"><i class="mdi mdi-floppy"></i> Opslaan</button>
      </div>
    </div>
  </form>
</div>