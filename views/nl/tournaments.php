<div class="container">

  <center>
    <h1><b>Toernooien overzicht</b></h1>
  </center>
  <hr>

  <div class="text-right">
    <a href="<?=DEFAULT_DIR; ?>/nl/tournament" class="btn btn-success">Toernooi toevoegen</a>
  </div>

  <div class="row m-t-30">

    <div class="col-12">

      <?php if (empty((array)$data['tournaments'])) { ?>
        <span class="alert alert-danger btn-block text-center"> Geen wedstrijden kunnen vinden. </span>
      <?php } else { ?>
        <table id="data-table" class="table table-striped table-hover">

          <thead>
            <th>Beschrijving</th>
            <th>Start datum</th>
            <th class="no-sort text-right">Opties</th>
          </thead>

          <tbody>
            <?php foreach ($data['tournaments'] as $tournament) { ?>
              <tr>
                <td><span data-toggle="tooltip" data-placement="top" title="<?=$tournament->tournament_description; ?>"><?=afkorten($tournament->tournament_description, 50); ?></span></td>
                <td data-order="<?=strtotime($tournament->tournament_date); ?>"><?=engToNldDateNames(date('D d-m-Y', strtotime($tournament->tournament_date))); ?></td>
                <td class="text-right">
                  <a class="btn btn-sm btn-primary text-white" href="<?=DEFAULT_DIR; ?>/nl/results/<?=$tournament->tournament_id; ?>" data-toggle="tooltip" data-placement="top" title="Uitslagen rondes"><i class="mdi mdi-file-certificate"></i> Uitslagen</a>
                  <a class="btn btn-sm btn-info text-white" href="<?=DEFAULT_DIR; ?>/nl/players/<?=$tournament->tournament_id; ?>" data-toggle="tooltip" data-placement="top" title="Aanmeldingen overzicht"><i class="mdi mdi-account-group"></i> Aanmeldingen</a>
                  <a class="btn btn-sm btn-warning text-white" href="<?=DEFAULT_DIR; ?>/nl/tournament/<?=$tournament->tournament_id; ?>/edit" data-toggle="tooltip" data-placement="top" title="Toernooi aanpassen"><i class="mdi mdi-pencil"></i></a>
                </td>
              </tr>
            <?php } ?>
          </tbody>

        </table>
      <?php } ?>

    </div>

  </div>

</div>