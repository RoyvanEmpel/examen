<div class="container">

  <center>
    <h1><b>Uitslagen overzicht</b></h1>
  </center>
  <hr>

  <div class="row">
    <div class="col-6">
      <a class="btn btn-danger" href="<?=DEFAULT_DIR; ?>/nl/results/<?=$data['tournament_id']; ?>">Terug naar uitslagen</a>
    </div>
  </div>

    <div class="row m-t-30">

      <div class="col-12">

      <?php if (!empty((array)$data['matches'])) { ?>
        <div class="container">
          <?php foreach ($data['matches'] as $index => $round) { ?>
            <hr>
            <center><h1>Ronde <?=$index+1; ?></h1></center>
            <div class="row">
              <?php foreach ($round as $match) { ?>
                <div class="col" style="border: black solid 1px;">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" disabled type="checkbox" id="<?=$match->match_id; ?>_1" value="<?=$match->player_1_id; ?>" <?=($match->player_1_id == $match->winner_id && !empty($match->winner_id) ? 'checked' : ''); ?>>
                    <label class="form-check-label" for="<?=$match->match_id; ?>_1"><?=$match->firstname_1 ?? "Joker"; ?> <?=$match->tussenvoegsel_1; ?> <?=$match->lastname_1; ?></label>
                  </div><br>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" disabled type="checkbox" id="<?=$match->match_id; ?>_2" value="<?=$match->player_2_id; ?>"<?=($match->player_2_id == $match->winner_id && !empty($match->winner_id) ? 'checked' : ''); ?>>
                    <label class="form-check-label" for="<?=$match->match_id; ?>_2"><?=$match->firstname_2 ?? "Joker"; ?> <?=$match->tussenvoegsel_2; ?> <?=$match->lastname_2; ?></label>
                  </div>
                </div>
              <?php } ?>
            </div>
          <?php } ?>
          <hr>
          <?php
            if (count((array)$data['matches'][count((array)$data['matches'])-1]) == 1) { // Checken of het de laatste ronde is en zoja dan laat de winnaar zien.
              $winner = $data['matches'][count((array)$data['matches'])-1]->{0};
          ?>
            <center><h1>Winnaar</h1></center>
            <div class="row">
              <div class="col alert alert-warning" style="border: gold solid 1px;">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" disabled type="checkbox" id="<?=$winner->match_id; ?>_1" checked>
                  <label class="form-check-label" for="<?//; ?>_1"><?=$winner->firstname_w ?? "Joker"; ?> <?=$winner->tussenvoegsel_w; ?> <?=$winner->lastname_w; ?></label>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      <?php } else { ?>
        <hr>
        <span class="alert alert-danger btn-block">Geen wedstrijden gevonden</span>
      <?php } ?>

      </div>

    </div>

</div>