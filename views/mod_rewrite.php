<!-- Dit bestand is alleen van toepassing in het geval dat de instellingen in apache niet goed zijn geconfigureerd. -->


<style>
  .background {
    background-image: url('content/images/background.jpg');
    background-repeat: no-repeat;
    background-size: cover;
    filter: blur(8px);
    width: 100%;
    height: 100%;
  }

  .container{
    background: #FFF;
    left: 50%;
    position: absolute;
    top: 45%;
    -ms-transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    z-index: 2;
  }
</style>
<div class="background"></div>
<div class="container alert m-t-40">
  <div class="row">
    <div class="col-12">
      <h1 class="text-center alert alert-danger">APACHE CONFIG ERROR</h1>
      <hr>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
      <h3>Hallo gebruiker, het lijkt erop dat jouw config van apache niet goed staat ingesteld! Om ervoor te zorgen dat het systeem de juiste functions aanroept moet mod_rewrite goed worden aangezet!</h3>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
      <p>In de <strong>C:\xampp\apache\httpd.conf</strong></p>

      <p>Verifieer dat <strong>LoadModule rewrite_module modules/mod_rewrite.so</strong> geen <strong>#</strong> ervoor heeft.<br />

        <code>#LoadModule rewrite_module modules/mod_rewrite.so</code><br>
        Verander je dus in <br>
        <code>LoadModule rewrite_module modules/mod_rewrite.so</code>
        <hr>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
      <p>Ook moet er bij het onderstaande <strong>None</strong> => <strong>All</strong></p>

      <p><strong><code>&lt;Directory "C:/xampp/htdocs"&gt;</code></strong><br />
        &nbsp;&nbsp;&nbsp;&nbsp;<code>Options Indexes FollowSymLinks Includes ExecCGI</code><br />
        &nbsp;&nbsp;&nbsp;&nbsp;<code>AllowOverride None</code><br />
        &nbsp;&nbsp;&nbsp;&nbsp;<code>Require all granted</code><br />
        <strong><code>&lt;/Directory&gt;</code></strong>
      </p>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
      <b>Wordt dus</b> <br><br>
      <p><strong><code>&lt;Directory "C:/xampp/htdocs"&gt;</code></strong><br />
        &nbsp;&nbsp;&nbsp;&nbsp;<code>Options Indexes FollowSymLinks Includes ExecCGI</code><br />
        &nbsp;&nbsp;&nbsp;&nbsp;<code>AllowOverride All</code><br />
        &nbsp;&nbsp;&nbsp;&nbsp;<code>Require all granted</code><br />
        <strong><code>&lt;/Directory&gt;</code></strong>
      </p>
    </div>
  </div>
  <hr>


  <div class="row">
    <div class="col-12">
      <div class="text-right">
        <span class="text-secondary">&copy; 2020-<?= date('Y'); ?> - Roy van Empel</span>
      </div>
    </div>
  </div>
</div>