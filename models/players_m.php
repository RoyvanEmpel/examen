<?php
include_once('controllers/db.php');

class Players_m {
  
  private $db;
  private $player_role_id;

  function __construct()
  {
    $this->db = new DB();
    $this->player_role_id = ($this->db->query("SELECT * FROM `roles` WHERE `role` = 'Speler'")->row())->role_id;
  }

  public function getAll($tournament_id) // Alle Spelers Ophalen
  {
    $query = "SELECT `p`.`player_id`, `p`.`firstname`, `p`.`tussenvoegsel`, `p`.`lastname`, `p`.`school_id`, `s`.`school_name`, `a`.`tournament_id`
              FROM `aanmeldingen` a
              JOIN `players` `p` ON `a`.`player_id` = `p`.`player_id`
              LEFT JOIN `schools` `s` ON `p`.`school_id` = `s`.`school_id`
              WHERE `a`.`tournament_id` = $tournament_id";

    return $this->db->query($query)->result();
  }

  public function getWinners($tournament_id, $round) // Alle Match winnaars Ophalen Van De Huidige Ronde In Het Toernooi
  {
    $query = "SELECT `winner_id`
              FROM `matches` `m`
              WHERE `winner_id` IS NOT NULL 
              AND `tournament_id` = $tournament_id
              AND `round` = $round";

    return $this->db->query($query)->result();
  }

  public function getAllDummyd($tournament_id, $round) // Spelers met een Dummy/Joker Ophalen Van De Huidige Ronde In Het Toernooi
  {
    $query = "SELECT `player_1`, `player_2`
              FROM `matches` `m`
              WHERE `winner_id` IS NULL
              AND (player_1 IS NULL OR player_2 IS NULL) 
              AND `tournament_id` = $tournament_id
              AND `round` = $round";

    return $this->db->query($query)->result();
  }

  public function get($player_id) // Een Speler Ophalen
  {
    $query = "SELECT `p`.`firstname`, `p`.`tussenvoegsel`, `p`.`lastname`, `p`.`school_id`
              FROM `players` `p`
              WHERE `player_id` = $player_id";

    return $this->db->query($query)->row();
  }

  public function addPlayer($input) // Speler Toevoegen
  {
    $array = [
      "firstname" => $input['firstname'],
      "tussenvoegsel" => $input['tussenvoegsel'] ?? "",
      "lastname" => $input['lastname'],
      "school_id" => $input['school_id'],
    ];

    if (isset($input['player_id'])) {
      $array['player_id'] = $input['player_id'];
    }

    $player_id = $this->db->insert('players', $array);

    $this->db->insert('aanmeldingen', [
      "tournament_id" => $input['tournament_id'],
      "player_id" => $player_id,
    ]);
  }

  public function editPlayer($player_id, $input) // Speler Aanpassen
  {
    $this->db->update('players', [
      "firstname" => $input['firstname'],
      "tussenvoegsel" => $input['tussenvoegsel'] ?? "",
      "lastname" => $input['lastname'],
      "school_id" => $input['school_id'],
    ], 
    [
      'player_id' => $player_id
    ]);
  }

  public function deletePlayer($player_id, $tournament_id) // Speler verwijderen
  {
    $this->db->delete('aanmeldingen', ['player_id' => $player_id, 'tournament_id' => $tournament_id]);
    $this->db->delete('players', ['player_id' => $player_id]);
  }

}