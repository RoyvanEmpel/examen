<?php
include_once('controllers/db.php');

class Matches_m {
  
  public $db;

  function __construct()
  {
    $this->db = new DB();
  }

  public function getAll() // alle matches ophalen
  {
    $query = "SELECT `m`.`round`, `p1`.`firstname` as firstname_1, `p1`.`lastname` as lastname_1, `p2`.`firstname` as firstname_2, `p2`.`lastname` as lastname_2, `m`.`player_1_score`, `m`.`player_2_score`, `w`.`firstname` as firstname_w, `w`.`lastname` as lastname_w
              FROM `matches` m
              JOIN `tournaments` t ON `m`.`tournament_id` = `t`.`tournament_id`
              JOIN `players` p1 ON `m`.`player_1` = `p1`.`player_id`
              JOIN `players` p2 ON `m`.`player_2` = `p2`.`player_id`
              JOIN `players` w ON `m`.`winner_id` = `w`.`player_id`
              ";

    return $this->db->query($query)->result();
  }

  public function get($match_id) // Specifieke matche ophalen
  {
    $query = "SELECT `m`.`round`, `p1`.`player_id`, `p2`.`player_id`, `w`.`player_id`, `m`.`player_1_score`, `m`.`player_2_score`
              FROM `matches` m
              JOIN `tournaments` t ON `m`.`tournament_id` = `t`.`tournament_id`
              JOIN `players` p1 ON `m`.`player_1` = `p1`.`player_id`
              JOIN `players` p2 ON `m`.`player_2` = `p2`.`player_id`
              JOIN `players` w ON `m`.`winner_id` = `w`.`player_id`
              WHERE match_id = $match_id";

    return $this->db->query($query)->row();
  }

  public function addMatch($input) // match toevoegen
  {
    $matches = [
      "tournament_id" => $input['tournament_id'],
      "round" => $input['round'],
    ];

    if (isset($input['player_1'])) {
      $matches["player_1"] = $input['player_1'];
    }
    if (isset($input['player_2'])) {
      $matches["player_2"] = $input['player_2'];
    }

    $this->db->insert('matches', $matches);
  }

  public function editMatch($match_id, $input) // match aanpassen
  {
    $this->db->update('matches', [
      "tournament_id" => $input['tournament_id'],
      "round" => $input['round'],
      "player_1" => $input['player_1'],
      "player_2" => $input['player_2'],
    ], 
    [
      'match_id' => $match_id
    ]);
  }

  public function editScores($match_id, $input) // Score aanpassen
  {
    $this->db->update('matches', [
      "player_1_score" => $input['player_1_score'],
      "player_2_score" => $input['player_2_score'],
      "winner_id" => $input['winner_id'],
    ], 
    [
      'match_id' => $match_id
    ]);
  }

  public function getRound($tournament_id) // Huidige ronde ophalen van het tournament
  {
    $query = "SELECT `m`.`round`
              FROM `matches` `m`
              WHERE `m`.`tournament_id`=$tournament_id
              GROUP BY `m`.`round`
              ORDER BY `m`.`round` DESC
              LIMIT 1
            ";

    return $this->db->query($query)->row();
  }

  public function getByTournament($tournament_id, $round=1) // Alle Matches ophalen a.d.h.v. Ronde en Tournament
  {
    $query= "SELECT `m`.`round`, 
                    `m`.`match_id`, 

                    `m`.`player_1` AS player_1_id,
                    `p1`.`firstname` as firstname_1, 
                    `p1`.`tussenvoegsel` as tussenvoegsel_1, 
                    `p1`.`lastname` as lastname_1,
                    `m`.`player_1_score`, 

                    `m`.`player_2` AS player_2_id, 
                    `p2`.`firstname` as firstname_2, 
                    `p1`.`tussenvoegsel` as tussenvoegsel_2, 
                    `p2`.`lastname` as lastname_2, 
                    `m`.`player_2_score`, 

                    `w`.`player_id` as winner_id, 
                    `w`.`firstname` as firstname_w, 
                    `w`.`tussenvoegsel` as tussenvoegsel_w, 
                    `w`.`lastname` as lastname_w
              FROM `matches` m
              JOIN `tournaments` t ON `m`.`tournament_id` = `t`.`tournament_id`
              LEFT JOIN `players` p1 ON `m`.`player_1` = `p1`.`player_id`
              LEFT JOIN `players` p2 ON `m`.`player_2` = `p2`.`player_id`
              LEFT JOIN `players` w ON `m`.`winner_id` = `w`.`player_id`
              WHERE `m`.`tournament_id` = $tournament_id
              AND `m`.`round` = $round";

    return $this->db->query($query)->result();
  }

}