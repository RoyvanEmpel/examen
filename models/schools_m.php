<?php
include_once('controllers/db.php');

class Schools_m {
  
  private $db;

  function __construct()
  {
    $this->db = new DB();
  }

  public function getAll()
  {
    $query = "SELECT *, (SELECT COUNT(`user_id`) FROM `users` WHERE `school_id` = `s`.`school_id`) as amount_players
              FROM `schools`s 
              WHERE `active` = 1";

    return $this->db->query($query)->result();
  }

  public function get($school_id)
  {
    $query = "SELECT * 
              FROM `schools` 
              WHERE school_id = $school_id";

    return $this->db->query($query)->row();
  }

  public function addSchool($input)
  {
    $this->db->insert('schools', [
      "school_name" => $input['school_name'],
    ]);
  }

  public function editSchool($school_id, $input)
  {
    $this->db->update('schools', [
      "school_name" => $input['school_name'],
    ], 
    [
      'school_id' => $school_id
    ]);
  }

  public function delSchool($school_id)
  {
    $this->db->update('schools', [
      "active" => 0,
    ], 
    [
      'school_id' => $school_id
    ]);
  }

}