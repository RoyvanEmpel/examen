<?php
include_once('controllers/db.php');

class Tournaments_m {
  
  public $db;

  function __construct()
  {
    $this->db = new DB();
  }

  public function getAll() // Alle tournamenten ophalen
  {
    return $this->db->query("SELECT * FROM `tournaments`")->result();
  }

  public function get($tournament_id) // Een tounament ophalen
  {
    $query = "SELECT * 
              FROM `tournaments`
              WHERE `tournament_id` = $tournament_id";

    $tournament = $this->db->query($query)->row();

    $tournament->matches = $this->matches_m->getByTournament($tournament->tournament_id);

    return $this->db->query($query)->row();
  }

  public function addTournament($input) // Toernament Toevoegen
  {
    $this->db->insert('tournaments', [
      "tournament_description" => $input['tournament_description'],
      "tournament_date" => $input['tournament_date'],
    ]);
  }

  public function editTournament($tournament_id, $input) // Toernament Aanpassen
  {
    $update = [
      "tournament_description" => $input['tournament_description'],
      "tournament_date" => $input['tournament_date'],
    ];

    $this->db->update('tournaments', $update, [ 'tournament_id' => $tournament_id ]);
  }

}