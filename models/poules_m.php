<?php
include_once('controllers/db.php');

class Poules_m {
  
  public $db;

  function __construct()
  {
    $this->db = new DB();
  }

  public function getAll()
  {
    $query = "SELECT `p`.*, `t`.`tournament_name`
              FROM `poules` p
              JOIN `tournaments` t ON `p`.`tournament_id` = `t`.`tournament_id`";

    $poules = $this->db->query($query)->result();
    foreach ($poules as &$poule) {
      $poule->users = $this->getUsers($poule->poule_id);
    }

    return $poules;
  }

  public function getActive()
  {
    $query = "SELECT `p`.*, `t`.`tournament_name`
              FROM `poules` p
              JOIN `tournaments` t ON `p`.`tournament_id` = `t`.`tournament_id`
              WHERE `poule_active` = 1";

    $poules = $this->db->query($query)->result();
    foreach ($poules as &$poule) {
      $poule->users = $this->getUsers($poule->poule_id);
    }

    return $poules;
  }

  public function get($poule_id)
  {
    $query = "SELECT `p`.*, `t`.`tournament_name`
              FROM `poules` p
              JOIN `tournaments` t ON `p`.`tournament_id` = `t`.`tournament_id`
              WHERE `poule_id` = $poule_id";

    $poules = $this->db->query($query)->result();
    foreach ($poules as &$poule) {
      $poule->users = $this->getUsers($poule->poule_id);
    }

    return $poules;
  }

  public function getUsers($poule_id)
  {
    $query = "SELECT pu.*, u.user_id, u.firstname, u.lastname, u.email
              FROM `poule_users` `pu`
              JOIN `users` `u` ON `pu`.`user_id` = `u`.`user_id`
              WHERE `poule_id` = $poule_id";

    return $this->db->query($query)->result();
  }

  public function addPoule($input)
  {
    $this->db->insert('poules', [
      "tournament_id" => $input['tournament_id'],
      "poule_name" => $input['poule_name'],
      "poule_description" => $input['poule_description'],
      "poule_active" => (isset($input['poule_active']) ? 1 : 0),
    ]);
  }

  public function editPoule($poule_id, $input)
  {
    $this->db->update('poules', [
      "tournament_id" => $input['tournament_id'],
      "poule_name" => $input['poule_name'],
      "poule_description" => $input['poule_description'],
      "poule_active" => (isset($input['poule_active']) ? 1 : 0),
    ], 
    [
      'poule_id' => $poule_id
    ]);
  }

  public function addUserToPoule($user_id, $poule_id)
  {
    $this->db->insert('poule_users', [
      'user_id' => $user_id,
      'poule_id' => $poule_id,
    ]);
  }

  public function removeUserFromPoule($user_id, $poule_id)
  {
    $this->db->delete('poule_users', [
      'user_id' => $user_id,
      'poule_id' => $poule_id,
    ]);
  }

  public function removeAllUsersFromPoule($poule_id)
  {
    $this->db->delete('poule_users', [
      'poule_id' => $poule_id,
    ]);
  }

}